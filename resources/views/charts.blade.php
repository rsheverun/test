@extends('layouts.tplAdmin')
@section('content')
<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Charts</li>
      </ol>
      @include('components.areaChart')
     <div class="row">
     	<div class="col-lg-8">
     		@include('components.barChartCard')
     	</div>
     	<div class="col-lg-4">
     		@include('components.pieChardCard')
     	</div>
     </div>
	
	

@endsection