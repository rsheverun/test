@extends('layouts.tplAdmin')
@section('content')
<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Cards</li>
      </ol>
      <h1>Cards</h1>
      <hr>
	@include('components.cards')
@endsection