<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('test', 'Test@test')->name('test');

Auth::routes();


Route::get('/', 'HomeController@index')->name('home');
Route::get('/charts', 'HomeController@charts')->name('charts');
Route::get('tables', 'HomeController@tables')->name('tables');
Route::get('navbar', 'HomeController@navbar')->name('navbar');
Route::get('cards', 'HomeController@cards')->name('cards');
Route::get('map', 'HomeController@map')->name('map');


